﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Upgrade", menuName = "ScriptableObjects/Create New Upgrade", order = 1)]

public class Upgrade : ScriptableObject
{
    [SerializeField]
    public string upgradeName;
    [SerializeField]
    public string description;
    [SerializeField]
    public float boostPerSecond;
    [SerializeField]
    public float clickBoost;
}
