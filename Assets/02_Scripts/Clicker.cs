﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Clicker : MonoBehaviour
{
    GameObject spaceShip;
    float distance;
    [SerializeField]
    GameObject distanceCounter;
    string displayedDistance;
    float clickIncreasement = 1f;

    public float Distance
    {
        get { return distance; }
        set { distance = value; }
    }

    public float ClickIncreasement
    {
        get { return clickIncreasement; }
        set { clickIncreasement = value; }
    }

    void Start()
    {
        spaceShip = GameObject.Find("Cube");
        distanceCounter = GameObject.Find("DistanceCounter");
    }

    void Update()
    {
        distanceCounter.GetComponent<TextMeshProUGUI>().text = "Distance: " + displayedDistance;
        displayedDistance = distance.ToString("F0");

        
        /*Touch[] myTouches = Input.touches;

        for(int i = 0; i < Input.touchCount; i++)
        {
            if (Input.GetTouch(i).phase == TouchPhase.Began && Input.touchCount <= 4)
            {
                distance += clickIncreasement;
                Debug.Log(distance);
            }
        }*/
    }
    
    
    
    private void OnMouseDown()
    {
        distance += ClickIncreasement;
        Debug.Log(distance);
    }
}
