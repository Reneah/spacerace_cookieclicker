﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeButton : MonoBehaviour
{
    [SerializeField]
    Upgrade myUpgrade;
    [SerializeField]
    Clicker myClicker;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActivateUpgrade()
    {
        myClicker.Distance += myUpgrade.boostPerSecond;
        myClicker.ClickIncreasement += myUpgrade.clickBoost;
    }
}
